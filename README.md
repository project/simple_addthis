CONTENTS OF THIS FILE
----------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


Introduction
============

The Simple Addthis module allows users to share content across every social
media platform and cross-promote the web site.

 * For a full description of the module visit:
   https://www.drupal.org/project/simple_addthis
   or
   http://www.addthis.com

 * To submit bug reports and feature suggestions, or to track changes visit:
     https://drupal.org/project/issues/simple_addthis


Requirements
============

This module requires no modules outside of Drupal Core.

* The AddThis share button requires JavaScript to run.


Installation
============

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
=============

    1. Navigate to Administration > Extend and enable the module.
    2. In the Manage administrative menu, navigate to Structure > Block layout
       (admin/structure/block).
    3. Place the Block in the desired region.


MAINTAINERS
===========

Current maintainers:
 * fazni - https://www.drupal.org/u/fazni
